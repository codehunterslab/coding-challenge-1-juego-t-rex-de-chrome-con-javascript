document.addEventListener('keydown', function(evento){
	if(evento.keyCode == 32){
		if(juego.finalizado == false){
			salto.play();
			saltar();
		}else{
			juego.finalizado = false;
			juego.puntos = 0;
			juego.velocidad = 10;
			nube.velocidad = 3;
			nube.x = ancho+100;
			cactus.x = ancho+100;
			cargarImagenes();
		}

	}
})

//Variables
var fps = 40;

var ancho = 680;
var alto = 300;

var lienzo, contexto;

//Archivos de audio
var salto = new Audio();
var punto = new Audio();
var final = new Audio();
salto.src = "audio/salto.mp3";
punto.src = "audio/punto.mp3";
final.src = "audio/final.mp3";

var juego = {velocidad:10, puntos:0, finalizado:false};
var suelo = {x: 0, y: 200}
var trex = {x:100, y:suelo.y, gravedad:3, salto:25, vy:0, vymax:8, saltando:false};
var cactus = {x: ancho+10, y:suelo.y};
var nube = {x: 250, y:100, velocidad:3};

function cargarImagenes(){
	imgSuelo = new Image();
	imgTRex = new Image();
	imgCactus = new Image();
	imgNube = new Image();

	imgJuegoTerminado = new Image();
	imgReinicio = new Image();

	imgSuelo.src = "img/suelo.png";
	imgTRex.src = "img/trex.png";
	imgCactus.src = "img/cactus.png";
	imgNube.src = "img/nube.png";

	imgJuegoTerminado.src = "img/juegoterminado.png"
	imgReinicio.src = "img/reinicio.png"

}

function iniciarJuego(){
	lienzo = document.getElementById('milienzo');
	contexto = lienzo.getContext('2d');
	cargarImagenes();
}

function borrarLienzo(){
	lienzo.width = ancho;
	lienzo.height = alto;
}

//---------- TRex ----------//
function dibujarTRex(){
	contexto.drawImage(imgTRex, 0, 0, 89, 94, trex.x, trex.y, 50, 50);
}

function saltar(){
	trex.saltando = true;
	trex.vy = trex.salto;
}

//---------- Cactus ----------//
function controlarCactus(){
	if(cactus.x < 0){
		cactus.x = ancho+100;
		juego.puntos = juego.puntos+1;
		juego.velocidad = juego.velocidad+2;
	}else{
		cactus.x = cactus.x-juego.velocidad;
	}
}

function dibujarCactus(){
	contexto.drawImage(imgCactus, 0, 0, 34, 70, cactus.x, cactus.y, 25, 50);
}

//---------- Nube ----------//
function controlarNube(){
	if(nube.x < 0){
		nube.x = ancho+100;
	}else{
		nube.x = nube.x-nube.velocidad;
	}
}

function dibujarNube(){
	contexto.drawImage(imgNube, 0, 0, 92, 27, nube.x, nube.y, 92, 27);
}

//---------- Suelo ----------//
function controlarSuelo(){
	if(680 < suelo.x){
		suelo.x = 0;
	}else{
		suelo.x = suelo.x+juego.velocidad;
	}
}

function dibujarSuelo(){
	contexto.drawImage(imgSuelo, suelo.x, 0, 680, 44, 0, suelo.y, 680, 44);
}

//---------- Colision y puntaje ----------//

function colision(){
	if(100 <= cactus.x &&  cactus.x <= 150){
		if(suelo.y-25 <= trex.y){
			juego.finalizado = true;
			juego.velocidad = 0;
			nube.velocidad = 0;
			imgTRex.src = "img/trex-muerto.png"
		}
	}
}

function controlarPuntaje(){
	contexto.font = "bold 25px impact";
	contexto.fillStyle = "#535353";
	contexto.fillText(juego.puntos, 550,50);

	if(juego.finalizado == true){
		contexto.drawImage(imgJuegoTerminado, 0, 0, 381, 21, 150, 100, 381, 21);
		contexto.drawImage(imgReinicio, 0, 0, 75, 64, 310, 150, 60, 50);
	}
}

//---------- Gravedad ----------//
function gravedad(){
	if(trex.saltando == true){
		if(trex.y-trex.vy-trex.gravedad > suelo.y){
			trex.saltando = false;
			trex.vy = 0;
			trex.y = suelo.y;
		}else{
			trex.vy = trex.vy-trex.gravedad;
			trex.y = trex.y-trex.vy;
		}
	}
}

function principal(){
	borrarLienzo();
	gravedad();
	colision();

	controlarCactus();
	controlarNube();
	controlarSuelo();

	dibujarSuelo();
	dibujarNube();
	dibujarCactus();
	dibujarTRex();

	controlarPuntaje();
}

setInterval(function(){
	principal();
}, 1000/fps);